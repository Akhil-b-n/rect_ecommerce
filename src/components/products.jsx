import React from "react";

function Products({ products }) {
  if (!products || !products.image || !products.title || !products.price || !products.description) {
    console.error('Invalid products data:', products);
    return null; // Handle case when products or its properties are null or undefined
  }

  return (
    <div className="container mx-auto my-5">
      <article className="flex flex-col md:flex-row md:justify-between md:items-center gap-3">
        <div className="grid grid-cols-4 grid-rows-3 gap-2">
          {products.image && (
            <>
              <img className="row-start-1 row-end-2 col-start-1 col-end-2" src={products.image} alt={products.title} />
              <img className="row-start-2 row-end-3 col-start-1 col-end-2" src={products.image} alt={products.title} />
              <img className="row-start-3 row-end-4 col-start-1 col-end-2" src={products.image} alt={products.title} />
              <img className="row-start-1 row-end-4 col-start-2 col-end-5 w-full" src={products.image} alt={products.title} />
            </>
          )}
        </div>
        <div className="flex flex-col justify-between items-center gap-5">
          <h1>{products.title}</h1>
          <span>
            MRP ₹<span>{products.price}</span>
          </span>
          <ul className="variants flex gap-2">
            <li>
              <img className="w-7 h-7 rounded-full" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAABlBMVEXY2Njo6Oie6BDVAAABDUlEQVR4nO3PAQ0AIAzAsOPfNCoIGbQKtpnHrdsBxznsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77HPY57HPY57DPYZ/DPod9Dvsc9jnsc9jnsM9hn8M+h30O+xz2Oexz2Oewz2Gfwz6HfQ77Pjjcs3wA4s0RAgUAAAAASUVORK5CYII=" alt="variant" />
            </li>
            {/* Repeat for other variants */}
          </ul>
          <p>{products.description}{products.description}{products.description}{products.description}</p>
          <div className="flex flex-col items-center justify-between md:flex-row gap-4 md:gap-8">
            <span className="bg-green-700 text-white px-4 py-3 rounded-md hover:bg-green-600 hover:font-bold">Buy Now</span>
            <span className="bg-orange-500 text-white px-4 py-3 rounded-md hover:bg-orange-600 hover:font-bold">Add to cart</span>
          </div>
        </div>
      </article>
    </div>
  );
}

export default Products;
