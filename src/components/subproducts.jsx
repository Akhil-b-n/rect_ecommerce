import React from "react";

function Subproducts({ product }) {
  if (!product || !product.image || !product.title || !product.price) {
    console.error('Invalid product data:', product);
    return null; // Handle case when product or its properties are null or undefined
  }

  return (
    <article className="mx-auto my-5">
      <div className="subproducts flex flex-col items-center justify-between gap-4">
        <img src={product.image} alt={product.title} />
        <h3>{product.title}</h3>
        <span className="price">MRP <span>{product.price}</span></span>
      </div>
    </article>
  );
}

export default Subproducts;
