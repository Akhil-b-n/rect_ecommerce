import React, { useState } from "react";

function Header(){
    const [isOpen,setIsOpen] =useState(false);
    const Togglemenu= () =>{
        setIsOpen(!isOpen);
    }
    return(
        <header className="bg-violet-500">
            <section className="container mx-auto flex flex-row justify-between items-center p-6 ">
                <h1><a href="#">Wave Shop</a></h1>
                <nav>
                    <div className="md:hidden">
                        <button onClick={Togglemenu}>
                        <span className="material-symbols-outlined">
                            menu
                        </span>
                        </button>
                        {
                            isOpen &&(
                                <ul className="">
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">About</a></li>
                                    <li><a href="#">Products</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                            )
                        }
                    </div>
                   <div>
                    <ul className="hidden md:flex md:flex-row md:justify-between md:items-center gap-4">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Products</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                   </div>
                </nav>
            </section>     
        </header> 
    )
}
export default Header;