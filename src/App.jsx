import { useEffect, useState } from 'react';
import './App.css';
import Header from './components/Header';
import Products from './components/products';
import Subproducts from './components/subproducts';
import axios from 'axios';
import ErrorBoundary from './components/ErrorBoundary'; // Make sure to import the ErrorBoundary component

function App() {
  const [products, setProducts] = useState([]);
  const [error, setError] = useState(null);
  const [retryCount, setRetryCount] = useState(0);
  const maxRetries = 3;

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const response = await axios.get('https://fakestoreapi.com/products');
        setProducts(response.data);
        setError(null);
      } catch (err) {
        console.log('Error fetching products', err);
        setError('Failed to load products. Retrying...');
        if (retryCount < maxRetries) {
          setRetryCount(retryCount + 1);
        } else {
          setError('Failed to load products after multiple attempts.');
        }
      }
    };

    fetchProducts();
  }, [retryCount]);

  return (
    <ErrorBoundary>
      <Header />
      {error && <p className="error-message">{error}</p>}
      <Products products={products[0]} />
      <h3 className="container mx-auto my-10">Similar product</h3>
      <div className="container mx-auto">
        <div className="flex flex-col justify-between items-center gap-4 sm:grid md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 sm:grid-cols-2">
          {products.map((product, index) => (
            <Subproducts key={index} product={product} />
          ))}
        </div>
      </div>
    </ErrorBoundary>
  );
}

export default App;
